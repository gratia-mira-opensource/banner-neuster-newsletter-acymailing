<?php
/**
 * Bibelversanzeige! Module Entry Point
 * 
 * @package     Joomla.Tutorials
 * @subpackage  Modules
 * @license     GNU/GPL, see LICENSE.php
 * @link        https://gitlab.com/gratia-mira-opensource/banner-neuster-newsletter-acymailing
 * @copyright   Christoph J. Berger
 * mod_banner-neuster-newsletter-acymailing is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
/**
 * @var $params
*/

// No direct access
defined('_JEXEC') or die;

// Neu: Objekt-Orientiert.
// Die Bibelvers-Klasse laden.
require_once dirname(__FILE__) . '/helper.php';

// Ein neuer Bibelvers aus der Datenbank lesen
$oNeusterNewsletter = new NeusterNewsletter($params);

require JModuleHelper::getLayoutPath('mod_banner-neustenewsletter');
