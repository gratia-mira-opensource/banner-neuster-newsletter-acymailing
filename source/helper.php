<?php
/**
 * Klasse zum Anzeigen der neusten Newsletter! Joomla-Modul
 * 
 * @package     Joomla.Tutorials
 * @subpackage  Modules
 * @link        https://gitlab.com/gratia-mira-opensource/banner-neuster-newsletter-acymailing
 * @license     GNU/GPL, see LICENSE.php
 * @copyright   Christoph J. Berger
 * mod_banner-neuster-newsletter-acymailing is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 */
 
// Kein direkter Zugang
defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;

class NeusterNewsletter
{
	// Eigenschaften / Objekte
	/**
	 * Die HTML-Ausgabe mit verlinktem Bild.
	 * @var string
	 * @since 1.0.0
	 */
	public string $AusgabeBannerLink;
	
	// Die Klasse instanziieren, d.h. ein Objekt erzeugen (oder: Werte aus den Einstellungen holen)
	function __construct($params) {

		if($params->get('ExternerNewsletter')) {
			// Newsletter via JSON holen
			$this->holeLinkNeusterNewsletter($params);
		} else {
			// Newsletter aus der Datenbank holen
			$this->erstelleLinkNeusterNewsletter($params);
		}
    }

	/**
	 * Erstellt den Link, bzw. dem Banner für neusten Newsletter als HTML-Code.
	 * @param $params
	 * @since 1.0.0
	 * @version 1.1.0
	 */
	public function erstelleLinkNeusterNewsletter($params): void
	{
		$AcyMailingList = $params->get('AcyMailingList') ?: 0;
		
		// Die Datenbank-Verbindung holen
		$db = Factory::getContainer()->get('DatabaseDriver');
		
		// Ein Query-Objekt holen
		$query = $db->getQuery(true);	
		
		// Die Abfrage formulieren 
		$query->select(array('M.name As Name',  'M.id AS ID', 'S.mail_id AS MailID', 'S.send_date AS Datum', 'L.list_id AS List'));
		$query->from('#__acym_mail AS M');
		$query->innerJoin('#__acym_mail_stat AS S ON M.id = S.mail_id');
		$query->innerJoin('#__acym_mail_has_list AS L ON M.ID = L.mail_id');
		// Welche Liste
		$query->where('L.list_id = ' . $AcyMailingList);
		$query->order('Datum DESC');
		$query->setLimit('1');
		
		// Abfrage laden
		$db->setQuery($query);
 
		// Liste lesen
		$Resultat = $db->loadObjectList();
		
		// Link ausgeben
		$Name = mb_convert_encoding($Resultat[0]->Name, 'ISO-8859-1', 'UTF-8');
		$Banner = "<img src='" . JURI::base() . $params->get('Bannerbild') . "' alt='". $Name ."' width='100%'>";
		$AcyArchivLink = Route::_('index.php?option=com_acym&view=archive&layout=listing&task=view&id='.$Resultat[0]->ID . '&tmpl=template&userid=-',true,0,true);

		$Link = GMF_Layout::erstelleLink($AcyArchivLink,$Banner,'Newsletter »' . $Name . '» anzeigen','_blank');

		// Newsletter inklusive Bild zurückgeben
		if($AcyMailingList) {
			$this->AusgabeBannerLink = $Link;
		} else {
			$this->AusgabeBannerLink = 'Modul Banner neusten Newsletter: Bitte Liste auswählen!';
		}
		if(isset($_GET['NewsletterExterneSeite']) and $_GET['NewsletterExterneSeite'] = 'auslesen') {
			echo json_encode(urlencode($this->AusgabeBannerLink));
			exit;
		}
	}

	/**
	 * Holt den Link des neusten Banners
	 * und ersetzt gegebenenfalls das Bannerbild.
	 *
	 * @param $params
	 *
	 * @since unbekannt
	 * @version 1.0.3
	 */
	public function holeLinkNeusterNewsletter($params): void
	{

	// Die Referrer Policy anpassen, damit das JSON auch ausgelesen werden kann 
	header("Referrer-Policy: no-referrer-when-downgrade");
	
	$html = urldecode(json_decode(file_get_contents($params->get('Mutterlink') . '?NewsletterExterneSeite=auslesen')));
		
	if($params->get('Bannerbild')) {
		
		$Ersetzen = "<img src='" . JUri::base() . $params->get('Bannerbild') . "'";

		$html = preg_replace("/<img src='.*'/U",$Ersetzen,$html);
	}

	$this->AusgabeBannerLink = $html;
	}
	
}
