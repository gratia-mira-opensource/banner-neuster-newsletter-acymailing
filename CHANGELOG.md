# 1.1.0 26.03.2024
* Neu: Auf beliebigen Webseiten einsetzbar.
* Verbesserung: Veralteter Code ersetzt.
* Verbesserung: Verbesserungsvorschläge PHP Storm umgesetzt.