# Banner Neuster Newsletter AcyMailing
Verlinkt den neusten Newsletter, der via AcyMailing versendet wurde in einem Banner. Das Modul unterstützt auch ein JSON Ausgabe des Links (inklusive Bild).

# Voraussetzungen
Das Modul setzt voraus, dass AcyMailing installiert ist.
Auch das Gratia-Mira Framework muss installiert sein.